
        let str1 = '我喜歡攝影，喜歡 coding，喜歡設計，喜歡藍天帶幾朵白雲，喜歡 始終不放棄的自己。';
        let str2 = '也許我很幸運，你剛好也喜歡我的模樣。';
        let i = 0;
        let j = 0;
        function typing_1() {
            var divTyping = document.getElementById('typingText');
            if (i <= str1.length) {
                divTyping.innerHTML = str1.slice(0, i++) + '_';
                setTimeout('typing_1()', 200);//遞歸調用
            }
            else {
                divTyping.innerHTML = str1;//結束打字,移除 _ 光標
            }
        }
        function typing_2() {
            var divTyping = document.getElementById('typingText2');
            if (j <= str2.length) {
                divTyping.innerHTML = str2.slice(0, j++) + '_';
                setTimeout('typing_2()', 180);//遞歸調用
            }
            else {
                divTyping.innerHTML = str2;//結束打字,移除 _ 光標
            }
        }
        typing_1();
        setTimeout(function () { typing_2(); }, 10000);
